# Generator

The generator is a Java application which parses an UML diagram and produces Java classes out of it.

## Current features
 * Parse PlantUML class diagramm

## Usage
``java -jar generator.jar -source classdiagram.plantuml -destination /path/to/target/project``

| Parameter        | Description           
| ------------- |:-------------:|
| source      | The source UML file which will be parsed and used to generate the Java source code
| destination | The destination director where the generated source code will be put in

## Writing an UML diagram
### Recommended Tool
For creating PlantUML diagrams I recommend to use Visual Studio Code with the installed PlantUML diagram and Graphviz installed.

Description of the PlantUML syntax for class diagrams: http://plantuml.com/en/class-diagram

### Important information when creating a diagram

#### Fields
Fields always need to have the correct type set. When using a type you always need to add the full qualified name, so that the correct import can be generated.

Example: ``java.util.List<at.fhv.MyClass> list``
The type of the field and the name must be separated by a single blank.
