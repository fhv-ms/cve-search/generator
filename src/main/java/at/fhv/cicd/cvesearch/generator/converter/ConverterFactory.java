package at.fhv.cicd.cvesearch.generator.converter;

import at.fhv.cicd.cvesearch.generator.converter.classdiagram.ClassDiagramConverter;
import at.fhv.cicd.cvesearch.generator.converter.types.Class;
import net.sourceforge.plantuml.classdiagram.ClassDiagram;

/**
 * This factory delivers the correct converter for the given diagram type
 */
public class ConverterFactory {

    /**
     * Get a converter for a class diagram.
     * 
     * @param diagram the class diagram for which a converter should be loaded
     */
    public static Converter<Class> newDiagramConverter(ClassDiagram diagram) {
        return new ClassDiagramConverter(diagram);
    }
}