package at.fhv.cicd.cvesearch.generator.converter.types;

/**
 * Implementation of a field of a class. For enums only the name attribute will
 * be used, the other attributes will be null
 */
public class Field {
    private final String type;
    private final String name;
    private final VisibilityModifier visibility;

    public Field(String type, String name, VisibilityModifier visibility) {
        this.type = type;
        this.name = name;
        this.visibility = visibility;
    }

    public Field(String type, String name) {
        this(type, name, null);
    }

    public String getType() {
        return this.type;
    }

    public String getName() {
        return this.name;
    }

    public VisibilityModifier getVisibility() {
        return this.visibility;
    }
}