package at.fhv.cicd.cvesearch.generator;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Set;

import at.fhv.cicd.cvesearch.generator.converter.Converter;
import at.fhv.cicd.cvesearch.generator.converter.ConverterFactory;
import at.fhv.cicd.cvesearch.generator.converter.types.Class;
import net.sourceforge.plantuml.BlockUml;
import net.sourceforge.plantuml.FileFormat;
import net.sourceforge.plantuml.FileFormatOption;
import net.sourceforge.plantuml.SourceFileReader;
import net.sourceforge.plantuml.classdiagram.ClassDiagram;
import net.sourceforge.plantuml.cucadiagram.CucaDiagram;
import net.sourceforge.plantuml.cucadiagram.IEntity;
import net.sourceforge.plantuml.cucadiagram.LeafType;
import net.sourceforge.plantuml.cucadiagram.Link;
import net.sourceforge.plantuml.cucadiagram.LinkDecor;

public class Main {
    public static void main(String[] args) throws Exception {
        File source = new File(Main.class.getResource("test.plantuml").toURI());
        SourceFileReader reader = new SourceFileReader(source);
        reader.setFileFormatOption(new FileFormatOption(FileFormat.XMI_STANDARD));
        List<BlockUml> blocks = reader.getBlocks();
        for (BlockUml blockUml : blocks) {
            ClassDiagram uml = (ClassDiagram) blockUml.getDiagram();

            for (final IEntity entity : uml.getLeafsvalues()) {
                System.out.println(entity.getDisplay().get(0).toString());
                System.out.println(entity.getLeafType());
                System.out.println("FQN: " + entity.getCode().getFullName());
                System.out.println("  ----");
                for (Link link : getLinksButNotes(uml, entity)) {
                    if (link.getEntity2() == entity) {
                        System.out.println(getHtmlChiral(link, link.getEntity1().getCode().getFullName(),
                                link.getEntity2().getCode().getFullName()));
                    } else {
                        System.out.println(getHtml(link, link.getEntity2().getCode().getFullName(),
                                link.getEntity1().getCode().getFullName()));
                    }
                }
            }

            System.out.println("******************************");
            Converter<Class> converter = ConverterFactory.newDiagramConverter(uml);
            Set<Class> classes = converter.convert();
        }

    }

    private static Collection<Link> getLinksButNotes(CucaDiagram diagram, IEntity ent) {
        final List<Link> result = new ArrayList<Link>();
        for (Link link : diagram.getLinks()) {
            if (link.contains(ent) == false) {
                continue;
            }
            if (link.getEntity1().getLeafType() == LeafType.NOTE || link.getEntity2().getLeafType() == LeafType.NOTE) {
                continue;
            }
            result.add(link);
        }
        return Collections.unmodifiableList(result);
    }

    private static String getHtml(Link link, String ent1, String ent2) {
        final LinkDecor decor1 = link.getType().getDecor1();
        final LinkDecor decor2 = link.getType().getDecor2();

        if (decor1 == LinkDecor.NONE && decor2 == LinkDecor.NONE) {
            return ent1 + " is linked to " + ent2;
        }
        if (decor1 == LinkDecor.NONE && decor2 == LinkDecor.EXTENDS) {
            return ent1 + " is extended by " + ent2;
        }
        if (decor1 == LinkDecor.EXTENDS && decor2 == LinkDecor.NONE) {
            return ent1 + " extends " + ent2;
        }
        if (decor2 == LinkDecor.AGREGATION) {
            return ent1 + " is aggregated by  " + ent2;
        }
        if (decor1 == LinkDecor.AGREGATION) {
            return ent1 + " aggregates " + ent2;
        }
        if (decor2 == LinkDecor.COMPOSITION) {
            return ent1 + " is composed by " + ent2;
        }
        if (decor1 == LinkDecor.COMPOSITION) {
            return ent1 + " composes " + ent2;
        }
        if (decor1 == LinkDecor.NONE && decor2 == LinkDecor.ARROW) {
            return ent1 + " is navigable from  " + ent2;
        }
        if (decor1 == LinkDecor.ARROW && decor2 == LinkDecor.NONE) {
            return ent1 + " navigates to  " + ent2;
        }
        return ent1 + " " + decor1 + "-" + decor2 + " " + ent2;

    }

    private static String getHtmlChiral(Link link, String ent1, String ent2) {
        final LinkDecor decor1 = link.getType().getDecor1();
        final LinkDecor decor2 = link.getType().getDecor2();

        if (decor1 == LinkDecor.NONE && decor2 == LinkDecor.NONE) {
            return ent2 + " is linked to " + ent1;
        }
        if (decor1 == LinkDecor.NONE && decor2 == LinkDecor.EXTENDS) {
            return ent2 + " extends " + ent1;
        }
        if (decor1 == LinkDecor.EXTENDS && decor2 == LinkDecor.NONE) {
            return ent2 + " is extended by " + ent1;
        }
        if (decor2 == LinkDecor.AGREGATION) {
            return ent2 + " aggregates " + ent1;
        }
        if (decor1 == LinkDecor.AGREGATION) {
            return ent2 + " is aggregated by " + ent1;
        }
        if (decor2 == LinkDecor.COMPOSITION) {
            return ent2 + " composes " + ent1;
        }
        if (decor1 == LinkDecor.COMPOSITION) {
            return ent2 + " is composed by " + ent1;
        }
        if (decor1 == LinkDecor.NONE && decor2 == LinkDecor.ARROW) {
            return ent2 + " navigates to  " + ent1;
        }
        if (decor1 == LinkDecor.ARROW && decor2 == LinkDecor.NONE) {
            return ent2 + " is navigable from  " + ent1;
        }
        return ent1 + " " + decor1 + "-" + decor2 + " " + ent2;
    }
}