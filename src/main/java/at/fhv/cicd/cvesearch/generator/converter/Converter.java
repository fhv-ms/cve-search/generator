package at.fhv.cicd.cvesearch.generator.converter;

import java.util.Set;

/**
 * Interface for all converters
 */
public interface Converter<T> {

    /**
     * Convert the diagram
     */
    public Set<T> convert();
}