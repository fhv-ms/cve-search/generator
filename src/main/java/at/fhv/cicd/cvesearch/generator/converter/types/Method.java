package at.fhv.cicd.cvesearch.generator.converter.types;

import java.util.List;

/**
 * Implementation of a method of a class
 */
public class Method {
    private final String name;
    private final VisibilityModifier visibility;
    private final String returnType;
    private final List<Field> parameters;

    public Method(String name, VisibilityModifier visibility, String returnType, List<Field> parameters) {
        this.name = name;
        this.visibility = visibility;
        this.returnType = returnType;
        this.parameters = parameters;
    }

    public String getName() {
        return name;
    }

    public VisibilityModifier getVisibility() {
        return visibility;
    }

    public String getReturnType() {
        return returnType;
    }

    public List<Field> getParameters() {
        return parameters;
    }
}