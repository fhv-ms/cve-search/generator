package at.fhv.cicd.cvesearch.generator.converter.types;

import java.util.List;

import org.apache.commons.lang3.StringUtils;

/**
 * Implementation of a class, this can also be an enum or an interface (type
 * needs to be checked)
 */
public class Class {
    private final String namespace;
    private final String name;
    private final Type type;

    private final List<Field> fields;
    private final List<Method> methods;
    private final List<Link> links;

    public Class(String namespace, String name, Type type, List<Field> fields, List<Method> methods, List<Link> links) {
        this.namespace = namespace;
        this.name = name;
        this.type = type;
        this.fields = fields;
        this.methods = methods;
        this.links = links;
    }

    public String getNamespace() {
        return this.namespace;
    }

    public String getName() {
        return this.name;
    }

    public Type getType() {
        return this.type;
    }

    public String getFQN() {
        if (StringUtils.isEmpty(this.namespace)) {
            return this.name;
        }

        return this.namespace + "." + this.name;
    }

    public List<Field> getFields() {
        return this.fields;
    }

    public List<Method> getMethods() {
        return this.methods;
    }

    public List<Link> getLinks() {
        return this.links;
    }
}