package at.fhv.cicd.cvesearch.generator.converter.types;

import net.sourceforge.plantuml.cucadiagram.LeafType;

/**
 * Mapping of PlantUML net.sourceforge.plantuml.cucadiagram.LeafType
 */
public enum Type {
    CLASS, ABSTRACT_CLASS, INTERFACE, ENUM;

    /**
     * Get the Type of a PlantUML LeafType
     */
    public static Type fromLeafType(LeafType type) {
        switch (type) {
        case ABSTRACT_CLASS:
            return ABSTRACT_CLASS;
        case INTERFACE:
            return INTERFACE;
        case ENUM:
            return ENUM;
        case CLASS:
        default:
            return CLASS;
        }
    }
}