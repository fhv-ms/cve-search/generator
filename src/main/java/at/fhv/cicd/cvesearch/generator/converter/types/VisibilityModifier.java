package at.fhv.cicd.cvesearch.generator.converter.types;

/**
 * Mapping of the PlantUML net.sourceforge.plantuml.skin.VisibilityModifier
 */
public enum VisibilityModifier {
    PRIVATE, PROTECTED, PUBLIC;

    /**
     * Get the VisibilityModifier from a PlantUML VisibilityModifier
     */
    public static VisibilityModifier fromPlantUML(net.sourceforge.plantuml.skin.VisibilityModifier modifier) {
        if (modifier == null) {
            return PUBLIC;
        }

        switch (modifier) {
        case PRIVATE_FIELD:
        case PRIVATE_METHOD:
            return PRIVATE;
        case PROTECTED_FIELD:
        case PROTECTED_METHOD:
            return PROTECTED;
        case PUBLIC_FIELD:
        case PUBLIC_METHOD:
        default:
            return PUBLIC;
        }
    }
}