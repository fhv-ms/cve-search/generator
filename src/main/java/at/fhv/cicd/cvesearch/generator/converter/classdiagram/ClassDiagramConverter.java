package at.fhv.cicd.cvesearch.generator.converter.classdiagram;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;

import at.fhv.cicd.cvesearch.generator.converter.Converter;
import at.fhv.cicd.cvesearch.generator.converter.types.Class;
import at.fhv.cicd.cvesearch.generator.converter.types.Field;
import at.fhv.cicd.cvesearch.generator.converter.types.Method;
import at.fhv.cicd.cvesearch.generator.converter.types.Type;
import at.fhv.cicd.cvesearch.generator.converter.types.VisibilityModifier;
import net.sourceforge.plantuml.classdiagram.ClassDiagram;
import net.sourceforge.plantuml.cucadiagram.IEntity;
import net.sourceforge.plantuml.cucadiagram.Member;

public class ClassDiagramConverter implements Converter<Class> {
    private static Pattern METHOD_PATTERN = Pattern.compile("(.* )?([a-zA-Z0-9]*)\\((.*)\\)");
    private ClassDiagram diagram;
    private Map<Class, IEntity> classes;

    /**
     * Create a new class diagram converter
     * 
     * @param diagram The class diagram to convert
     */
    public ClassDiagramConverter(ClassDiagram diagram) {
        this.diagram = diagram;
        this.classes = new HashMap<>();
    }

    @Override
    public Set<Class> convert() {
        // walk throug all entities (classes, interfaces, etc)
        // needs to be done first so that the types generated here
        // can be used in methods and links
        for (final IEntity entity : this.diagram.getLeafsvalues()) {
            Class clazz = createClass(entity);
            this.classes.put(clazz, entity);
        }

        // enrich the entities with fields and methods
        for (Class clazz : this.classes.keySet()) {
            addFields(clazz, this.classes.get(clazz));
            addMethods(clazz, this.classes.get(clazz));
            addLinks(clazz, this.classes.get(clazz));
        }

        return this.classes.keySet();
    }

    /**
     * Create the class out of the given entity
     * 
     * @return the created class
     */
    private Class createClass(IEntity entity) {
        String namespace = (entity.getCode().getFullName().equals(entity.getCode().getLastPart())) ? ""
                : entity.getCode().getFullName().substring(0, entity.getCode().getFullName().lastIndexOf("."));
        String name = entity.getCode().getLastPart();
        Type type = Type.fromLeafType(entity.getLeafType());

        return new Class(namespace, name, type, new ArrayList<>(), new ArrayList<>(), new ArrayList<>());
    }

    /**
     * Add the fields to the given class
     * 
     * @param clazz  The class to which the fields should be added
     * @param entity The entity from which the fields will get loaded
     */
    private void addFields(Class clazz, IEntity entity) {
        for (Member m : entity.getBodier().getFieldsToDisplay()) {
            String name = m.getDisplay(false);

            if (clazz.getType().equals(Type.ENUM)) {
                clazz.getFields().add(new Field(null, name.toUpperCase(), null));
                continue;
            }

            VisibilityModifier visibility = VisibilityModifier.fromPlantUML(m.getVisibilityModifier());
            String type = m.getDisplay(false);

            clazz.getFields().add(new Field(type, name, visibility));
        }
    }

    /**
     * Add the methods to the given class
     * 
     * @param clazz  The class to which the methods should be added
     * @param entity The entity from which the methods will get loaded
     */
    private void addMethods(Class clazz, IEntity entity) {
        for (Member m : entity.getBodier().getMethodsToDisplay()) {
            String signature = m.getDisplay(false);

            String returnType = "";
            String name = "";
            String parameters = "";

            Matcher matcher = METHOD_PATTERN.matcher(signature);
            System.out.println(signature);
            if (matcher.find()) {
                returnType = matcher.group(1);
                name = matcher.group(2);
                parameters = matcher.group(3);
            }

            List<Field> params = new ArrayList<>();

            if (StringUtils.isNotEmpty(parameters)) {
                for (String parameter : parameters.split(",")) {
                    String[] p = parameter.split(" ");
                    params.add(new Field(p[0], p[1]));
                }
            }

            VisibilityModifier visibility = VisibilityModifier.fromPlantUML(m.getVisibilityModifier());
            clazz.getMethods().add(new Method(name, visibility, returnType, params));
        }

    }

    /**
     * Add the links to the given class
     * 
     * @param clazz  The class to which the links should be added
     * @param entity The entity from which the links will get loaded
     */
    private void addLinks(Class clazz, IEntity entity) {

    }

}