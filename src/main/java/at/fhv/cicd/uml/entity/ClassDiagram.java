package at.fhv.cicd.uml.entity;

import java.util.ArrayList;
import java.util.List;

import net.sourceforge.plantuml.cucadiagram.IEntity;

/**
 * Adapter for the {@link net.sourceforge.plantuml.classdiagram.ClassDiagram}
 */
public class ClassDiagram {
    private net.sourceforge.plantuml.classdiagram.ClassDiagram diagram;

    private List<Class> classes;

    /**
     * Create a new adapter for a
     * {@link net.sourceforge.plantuml.classdiagram.ClassDiagram}
     * 
     * @param diagram The diagram
     */
    public ClassDiagram(net.sourceforge.plantuml.classdiagram.ClassDiagram diagram) {
        this.diagram = diagram;
    }

    /**
     * Load the classes of the diagram
     */
    private void loadClasses() {
        this.classes = new ArrayList<>();

        // walk throug all entities (classes, interfaces, etc)
        for (final IEntity entity : this.diagram.getLeafsvalues()) {
            this.classes.add(new Class(entity));
        }
    }

    /**
     * Get the classes of this diagram
     */
    public List<Class> getClasses() {
        if (this.classes == null) {
            loadClasses();
        }

        return this.classes;
    }
}