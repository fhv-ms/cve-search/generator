package at.fhv.cicd.uml.entity;

import net.sourceforge.plantuml.cucadiagram.Member;

/**
 * Representation of a field of a class
 */
public class Field {
    private Member member;

    /**
     * Create a new Field
     * 
     * @param entity The wrapped field member
     */
    public Field(Member member) {
        this.member = member;
    }

    /**
     * Get the field name
     */
    public String getName() {
        String displayName = this.member.getDisplay(false);
        String[] splitted = displayName.split(" ");

        // enums do not have a type
        return splitted[splitted.length - 1];
    }

    /**
     * Get the field visibility
     */
    public VisibilityModifier getVisibility() {
        return VisibilityModifier.fromPlantUML(this.member.getVisibilityModifier());
    }

    /**
     * Get the type of the field
     */
    public String getType() {
        String displayName = this.member.getDisplay(false);
        return displayName.split(" ")[0];
    }
}