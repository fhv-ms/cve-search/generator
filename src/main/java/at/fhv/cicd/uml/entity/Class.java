package at.fhv.cicd.uml.entity;

import java.util.ArrayList;
import java.util.List;

import net.sourceforge.plantuml.cucadiagram.IEntity;
import net.sourceforge.plantuml.cucadiagram.Member;

/**
 * Adapter for a class
 */
public class Class {
    private IEntity entity;
    private List<Field> fields;

    /**
     * Create a new class
     * 
     * @param entity The wrapped class entity
     */
    public Class(IEntity entity) {
        this.entity = entity;
    }

    /**
     * Initialize the given diagram. This needs to be called before any other
     * actions
     */
    public void initialize() {
    }

    /**
     * Get the class package
     */
    public String getPackage() {
        // check if package is available
        if (!getFullQualifiedName().contains(this.entity.getCode().getSeparator())) {
            return "";
        }

        return getFullQualifiedName().substring(0,
                getFullQualifiedName().lastIndexOf(this.entity.getCode().getSeparator()));
    }

    /**
     * Get the name of the class
     */
    public String getName() {
        return this.entity.getCode().getLastPart();
    }

    /**
     * Get the full qualified name of the class
     */
    public String getFullQualifiedName() {
        return this.entity.getCode().getFullName();
    }

    /**
     * Get the type of the class
     */
    public Type getType() {
        return Type.fromLeafType(this.entity.getLeafType());
    }

    /**
     * Get the fields of the class
     */
    public List<Field> getFields() {
        if (this.fields == null) {
            loadFields();
        }

        return this.fields;
    }

    /**
     * Load the fields from the class
     */
    private void loadFields() {
        this.fields = new ArrayList<>();

        // walk through all member fields of this class
        for (Member m : this.entity.getBodier().getFieldsToDisplay()) {
            this.fields.add(new Field(m));
        }
    }
}