package at.fhv.cicd.uml.entity;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.io.IOException;
import java.net.URISyntaxException;

import org.junit.jupiter.api.Test;

import at.fhv.cicd.uml.entity.ClassDiagram;
import net.sourceforge.plantuml.BlockUml;

/**
 * Test class for {@link at.fhv.cicd.uml.entity.ClassDiagram}
 */
public class ClassDiagramTest extends AbstractTest {

    /**
     * Test a simple diagram
     * 
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void testDiagram() throws URISyntaxException, IOException {
        BlockUml uml = loadFirstUml("simple_diagram.plantuml");
        ClassDiagram diagram = new ClassDiagram((net.sourceforge.plantuml.classdiagram.ClassDiagram) uml.getDiagram());
        assertNotNull(diagram.getClasses());
        assertFalse(diagram.getClasses().isEmpty());
    }
}