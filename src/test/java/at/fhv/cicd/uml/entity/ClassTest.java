package at.fhv.cicd.uml.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import org.junit.jupiter.api.Test;

/**
 * Test class for {@link at.fhv.cicd.uml.entity.Class}
 */
public class ClassTest extends AbstractTest {

    /**
     * Test a simple diagram
     * 
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void testClasses() throws URISyntaxException, IOException {
        // check classes
        List<Class> classes = getClassesFromDiagram("classes.plantuml");
        assertEquals(2, classes.size());

        // Object test
        Class object = classes.get(0);
        assertEquals("Object", object.getName());
        assertEquals("", object.getPackage());
        assertEquals("Object", object.getFullQualifiedName());

        // ArrayList test
        Class arrayList = classes.get(1);
        assertEquals("ArrayList", arrayList.getName());
        assertEquals("at.fhv", arrayList.getPackage());
        assertEquals("at.fhv.ArrayList", arrayList.getFullQualifiedName());

    }

    /**
     * Test detection of different class types
     * 
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void testTypes() throws URISyntaxException, IOException {
        List<Class> classes = getClassesFromDiagram("classtypes.plantuml");
        assertEquals(6, classes.size());

        for (Class clazz : classes) {
            switch (clazz.getName()) {
            case "Collection":
                assertEquals(Type.INTERFACE, clazz.getType());
                continue;
            case "List":
                assertEquals(Type.INTERFACE, clazz.getType());
                continue;
            case "TimeUnit":
                assertEquals(Type.ENUM, clazz.getType());
                continue;
            case "AbstractCollection":
                assertEquals(Type.ABSTRACT_CLASS, clazz.getType());
                continue;
            case "AbstractList":
                assertEquals(Type.ABSTRACT_CLASS, clazz.getType());
                continue;
            case "ArrayList":
                assertEquals(Type.CLASS, clazz.getType());
                continue;
            default:
                fail("Unknow class");
            }
        }
    }
}