package at.fhv.cicd.uml.entity;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.List;

import net.sourceforge.plantuml.BlockUml;
import net.sourceforge.plantuml.SourceFileReader;

/**
 * Superclass for all unit tests
 */
public abstract class AbstractTest {

    /**
     * Load the given UML diagram and return the parsed UML blocks
     * 
     * @param file The file to load
     * @return The list of UML blocks parsed from the source file
     * @throws URISyntaxException Thrown on file loading errors
     * @throws IOException        Thrown on parsing errors
     */
    protected List<BlockUml> loadUml(String file) throws URISyntaxException, IOException {
        File source = new File(getClass().getClassLoader().getResource(file).getFile());
        SourceFileReader reader = new SourceFileReader(source);
        return reader.getBlocks();
    }

    /**
     * Load the given UML diagram and return only the first found UML block
     * 
     * @param file The file to load
     * @return The first found UML block
     * @throws IOException        Thrown on parsing errors
     * @throws URISyntaxException Thrown on file loading errors
     */
    protected BlockUml loadFirstUml(String file) throws URISyntaxException, IOException {
        List<BlockUml> umls = loadUml(file);

        if (umls == null || umls.isEmpty()) {
            return null;
        }

        return umls.get(0);
    }

    /**
     * Helper to get the classes from a diagram
     * 
     * @param diagramFile The diagram file to load and extract the classes
     * @return The list of classes of the given diagram
     * @throws IOException
     * @throws URISyntaxException
     */
    protected List<Class> getClassesFromDiagram(String diagramFile) throws URISyntaxException, IOException {
        // load uml
        BlockUml uml = loadFirstUml(diagramFile);
        ClassDiagram diagram = new ClassDiagram((net.sourceforge.plantuml.classdiagram.ClassDiagram) uml.getDiagram());

        return diagram.getClasses();
    }
}