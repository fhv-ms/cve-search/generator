package at.fhv.cicd.uml.entity;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;

/**
 * Test class for {@link at.fhv.cicd.uml.entity.Field}
 */
public class FieldTest extends AbstractTest {

    /**
     * Test the fields of a class
     * 
     * @throws IOException
     * @throws URISyntaxException
     */
    @Test
    public void testField() throws URISyntaxException, IOException {
        List<Class> classes = getClassesFromDiagram("fields.plantuml");
        Map<String, Class> c = new HashMap<>();

        for (Class clazz : classes) {
            c.put(clazz.getName(), clazz);
        }

        Class object = c.get("Object");
        assertEquals(0, object.getFields().size());

        Class arrayList = c.get("ArrayList");
        assertEquals(2, arrayList.getFields().size());
        Map<String, Field> arrayListFields = new HashMap<>();
        arrayList.getFields().forEach((Field f) -> arrayListFields.put(f.getName(), f));
        assertEquals("elementData", arrayListFields.get("elementData").getName());
        assertEquals("Object[]", arrayListFields.get("elementData").getType());
        assertEquals("list", arrayListFields.get("list").getName());
        assertEquals("at.fhv.List<Class>", arrayListFields.get("list").getType());

        Class timeUnit = c.get("TimeUnit");
        assertEquals(3, timeUnit.getFields().size());
        List<String> enums = new ArrayList<>();
        timeUnit.getFields().forEach((Field f) -> enums.add(f.getName()));
        assertTrue(enums.contains("DAYS"));
        assertTrue(enums.contains("HOURS"));
        assertTrue(enums.contains("MINUTES"));
    }

}